declare const global: any, imports: any;
export declare var powerProfileIndicatorExtensionInstance:any;
const Main = imports.ui.main;
const Config = imports.misc.config;
// @ts-ignore
const Me = imports.misc.extensionUtils.getCurrentExtension();

import * as Log from './modules/log';
import * as Panel from './modules/panel';
// @ts-ignore
import * as Resources from "./modules/resources";
import { IEnableableModule } from './interfaces/iEnableableModule';

export class powerProfileIndicatorExtension implements IEnableableModule {
  PowerProfileIndicatorInstance: any = null;
  systemMenu: any = null;

  constructor() {
    // nothing
  }

  enable() {
    this.systemMenu = this.getSystemMenu();

    if (!this.systemMenu) {
      Log.raw("init", "system menu is not defined");
      return false;
    }

    if (this.systemMenu._powerProfiles) {
      this.PowerProfileIndicatorInstance = new Panel.powerProfileIndicator();
    }
  }

  disable() {
    if (this.PowerProfileIndicatorInstance)
      this.PowerProfileIndicatorInstance.stop();

    if (this.PowerProfileIndicatorInstance._indicator)
      this.PowerProfileIndicatorInstance._indicator.destroy();

    this.PowerProfileIndicatorInstance._indicator = null;

    if (this.PowerProfileIndicatorInstance)
      this.PowerProfileIndicatorInstance.destroy();

    this.PowerProfileIndicatorInstance = null;
  }

  public populate() {
      // removing the power item and adding it afterwards
      // any ability to change the positions of childs?
      if (this.PowerProfileIndicatorInstance !== null) {
        if (parseInt(Config.PACKAGE_VERSION) < 43)
          if (this.systemMenu._power)
            this.systemMenu._indicators.remove_child(this.systemMenu._power);
  
        if (parseInt(Config.PACKAGE_VERSION) >= 43)
          if (this.systemMenu._system)
            this.systemMenu._indicators.remove_child(this.systemMenu._system);
  
        this.systemMenu._indicators.add_child(this.PowerProfileIndicatorInstance);
  
        if (parseInt(Config.PACKAGE_VERSION) < 43)
          if (this.systemMenu._power)
            this.systemMenu._indicators.add_child(this.systemMenu._power);
  
        if (parseInt(Config.PACKAGE_VERSION) >= 43)
          if (this.systemMenu._system)
            this.systemMenu._indicators.add_child(this.systemMenu._system);
      }
  }

  getSystemMenu() {
    if (parseInt(Config.PACKAGE_VERSION) < 43) {
      return Main.panel.statusArea.aggregateMenu;
    } else {
      return Main.panel.statusArea.quickSettings;
    }
  }
}

// @ts-ignore
function init() {
  powerProfileIndicatorExtensionInstance = new powerProfileIndicatorExtension();
  return powerProfileIndicatorExtensionInstance;
}
