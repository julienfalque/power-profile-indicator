declare const global: any, imports: any;

const { GLib } = imports.gi;

// @ts-ignore
const Me = imports.misc.extensionUtils.getCurrentExtension();

const Log = Me.imports.modules.log;

export class File {
  public static DBus(name: string) {
    let file = `${Me.path}/resources/dbus/${name}.xml`;
    try {
      let [_ok, bytes] = GLib.file_get_contents(file);
      if (!_ok) Log.raw(`Couldn't read contents of "${file}"`);
      return _ok ? imports.byteArray.toString(bytes) : null;
    } catch (e) {
      Log.raw(`Failed to load "${file}"`, e);
    }
  }
}
