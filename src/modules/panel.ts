declare const global: any, imports: any;
declare var powerProfileIndicatorExtensionInstance: any;

const { Gio, GObject } = imports.gi;
const { panelMenu } = imports.ui;
// @ts-ignore
const Me = imports.misc.extensionUtils.getCurrentExtension();

const Log = Me.imports.modules.log;
const Resources = Me.imports.modules.resources;

const PROFILE_ICONS: {
  "performance": string;
  "balanced": string;
  "power-saver": string;
} = {
  "performance": "power-profile-performance-symbolic",
  "balanced": "power-profile-balanced-symbolic",
  "power-saver": "power-profile-power-saver-symbolic"
};

export const powerProfileIndicator = GObject.registerClass(
  class powerProfileIndicator extends panelMenu.SystemIndicator {
    powerProfilesProxy: any = null; // type: Gio.DbusProxy (donno how to add)
    powerProfilesSignalProxy: any = null; // type: Gio.DbusProxy (donno how to add)
    connected: boolean = false;
    _state: string = "balanced";
    profiles: any = [];

    _init() {
      super._init();
      this.createProxy();
    }

    async createProxy() {
      this._indicator = this._addIndicator();
      this._indicator.icon_name = "power-profile-balanced-symbolic";
      this._indicator.visible = true;

      let xmlProfiles = Resources.File.DBus("net.hadess.PowerProfiles-0.10.1");
      this.powerProfilesProxy = await new Gio.DBusProxy.makeProxyWrapper(xmlProfiles)(
        Gio.DBus.system,
        "net.hadess.PowerProfiles",
        "/net/hadess/PowerProfiles",
        // @ts-ignore
        (proxy, e) => {
          try {
            this.connected = true;
            if (this.updateProfile())
              powerProfileIndicatorExtensionInstance.populate();
          } catch (error) {
            Log.raw('could not get ActiveProfile', error);
          }
        }
      );

      let xmlSignals = Resources.File.DBus(
        "net.hadess.PowerProfilesSignals-0.10.1"
      );
      this.powerProfilesSignalProxy = await new Gio.DBusProxy.makeProxyWrapper(
        xmlSignals
      )(
        Gio.DBus.system,
        "org.freedesktop.DBus.Properties",
        "/net/hadess/PowerProfiles",
        // @ts-ignore
        (proxy, e) => {
          try {
            proxy.connectSignal(
              "PropertiesChanged",
              // @ts-ignore
              (name: string = "", variant: any, profile: String[]) => {
                this.updateProfile();
              }
            );
          } catch (error) {
            Log.raw('error creating signal', error);
          }
        }
      );
    }

    updateProfile() {
      if (this.connected)
        try {
          this._state = this.powerProfilesProxy.ActiveProfile;

          if (typeof this._state !== 'string')
            return false;

          // @ts-ignore
          this._indicator.icon_name = PROFILE_ICONS[this._state];

          return true;
        } catch (error) {
          Log.raw("updateProfile", error);
        }

      return false;
    }

    stop() {
      this.connected = false;
      this.powerProfilesProxy = null;
      this.powerProfilesSignalProxy = null;
    }
  }
);
